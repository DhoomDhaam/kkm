package com.shahtek.kkm.auth;

import com.shahtek.kkm.config.AuthenticationProperties;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class AuthTokenManager {

    private AuthenticationProperties authenticationProperties;

    public AuthTokenManager(AuthenticationProperties authenticationProperties){
        log.info("action=create_auth_manager properties={}", authenticationProperties);
        this.authenticationProperties = authenticationProperties;
    }


    public String createToken(String id){

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        long expiry = nowMillis + authenticationProperties.getExpiry();

        Date expiryDate = new Date(expiry);

        return Jwts.builder().setId(id).
                setIssuedAt(now).
                setIssuer(authenticationProperties.getIssuer()).
                setExpiration(expiryDate).compact();
    }

    public Map<String,Object> validateToken(String token){
        Map<String,Object> headerMap = new HashMap<>();
        log.info("action=parsing_token input={}", token);
        try {
            Jwt<Header, Claims> claims = Jwts.parser().parseClaimsJwt(token);
            log.info("action=parsing_token claim={}", claims);
            headerMap.putAll(claims.getHeader());
            headerMap.putAll(claims.getBody());
        }catch(ExpiredJwtException ex){
            log.error("action=parsing_token EXPIRED_ERROR", ex);
            log.info("action details={}", ex.getClaims());
            headerMap.putAll(ex.getClaims());
            headerMap.putAll(ex.getHeader());
            headerMap.put("IS_EXPIRED", true);
        }catch(Exception ex){
            log.error("action=parsing_token error", ex);
        }
        return headerMap;
    }
}
