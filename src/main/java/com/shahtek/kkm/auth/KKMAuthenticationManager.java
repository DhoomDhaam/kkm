package com.shahtek.kkm.auth;

import com.shahtek.kkm.exception.AuthenticationFailureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KKMAuthenticationManager implements AuthenticationManager {

    public KKMAuthenticationManager(){

    }
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        log.info("ACTION=AUTHENTICATE authentication={}",authentication);

        try {
            if (authentication instanceof UsernamePasswordAuthenticationToken) {
                UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
                log.info("ACTION=AUTHENTICATE token={}", token);

                if ("a@a.com".equalsIgnoreCase(token.getName())) {
                    return authentication;
                } else {
                    log.error("ACTION=AUTHENTICATE password check failed");
                    throw new AuthenticationFailureException("failed");
                }
            }
        }catch(Exception ex){
            log.error("ACTION=AUTHENTICATE ERROR", ex);
            throw new AuthenticationFailureException("failed", ex);
        }
        return null;
    }
}
