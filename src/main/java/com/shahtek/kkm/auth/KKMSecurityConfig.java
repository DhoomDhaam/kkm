package com.shahtek.kkm.auth;

import com.shahtek.kkm.auth.filter.KKMAuthenticationFilter;
import com.shahtek.kkm.config.AuthenticationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j
public class KKMSecurityConfig extends WebSecurityConfigurerAdapter {
    private KKMAuthenticationFilter authenticationFilter;
    private KKMAuthenticationEntryPoint entryPoint;
    private KKMUserDetailsService userDetailsService;
    private AuthenticationProperties authenticationProperties;


    public KKMSecurityConfig(KKMAuthenticationFilter authenticationFilter, KKMAuthenticationEntryPoint entryPoint,
                             KKMUserDetailsService userDetailsService, AuthenticationProperties authenticationProperties){

        this.authenticationFilter = authenticationFilter;
        this.entryPoint = entryPoint;
        this.userDetailsService  = userDetailsService;
        this.authenticationProperties = authenticationProperties;

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("ACTION=SECURITY_CONFIG setting details service on builder={}", auth);
        auth.userDetailsService(userDetailsService);
    }


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        log.info("ACTION=SECURITY_CONFIG security={} properties={}", httpSecurity, authenticationProperties);

        if(authenticationProperties.isEnabled()) {

            httpSecurity.csrf().disable()
                        // TODO - add more urls which can be accessed
                        .authorizeRequests()
                        .antMatchers("/api/v1/authenticate").permitAll()
                        .antMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs").permitAll()
                        .anyRequest().authenticated().and()
                        .exceptionHandling().authenticationEntryPoint(entryPoint).and().sessionManagement()
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

            httpSecurity.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
        }else{
            log.info("ACTION=SECURITY_CONFIG_DISABLE security={} properties={}", httpSecurity, authenticationProperties);

            httpSecurity.csrf().disable().authorizeRequests().anyRequest().permitAll();
        }

        log.info("ACTION=SECURITY_CONFIG_DONE security={}", httpSecurity);

    }
}
