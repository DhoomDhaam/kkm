package com.shahtek.kkm.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
@Slf4j
public class KKMAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {
    @Override
    public void commence(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final AuthenticationException e) throws IOException, ServletException {
        log.info("ACTION=WUTHENTICATION_ENTRY req={}, res={}",httpServletRequest, httpServletResponse);
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorised");
    }
}
