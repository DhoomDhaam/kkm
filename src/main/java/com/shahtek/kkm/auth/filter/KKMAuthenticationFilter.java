package com.shahtek.kkm.auth.filter;

import com.shahtek.kkm.auth.AuthTokenManager;
import com.shahtek.kkm.auth.KKMUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;


@Component
@Slf4j
public class KKMAuthenticationFilter extends OncePerRequestFilter {

    private AuthTokenManager authTokenManager;
    private KKMUserDetailsService userDetailsService;

    public KKMAuthenticationFilter(AuthTokenManager authTokenManager, KKMUserDetailsService userDetailsService){
        this.authTokenManager = authTokenManager;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final FilterChain filterChain) throws ServletException, IOException {

        final String authorisationToken = httpServletRequest.getHeader("Authorization");

        log.info("ACTION=CHECK_AUTHORISATION authorisation_token={}", authorisationToken);

        Optional.ofNullable(authorisationToken).ifPresent(token -> {

            Map<String, Object> tokenValues = authTokenManager.validateToken(token);

            Object expiryFlag = tokenValues.get("IS_EXPIRED");
            log.info("ACTION=CHECK_AUTHORISATION TOKEN_EXPIRY={}", expiryFlag);

            String userName = String.valueOf(tokenValues.get("jti"));

            UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

            Authentication autFromContext = SecurityContextHolder.getContext().getAuthentication();

            log.info("ACTION=CHECK_AUTHORISATION AUTH_FROM_CONTEXT={}", autFromContext);

            if(!tokenValues.isEmpty() && SecurityContextHolder.getContext().getAuthentication() == null) {

                // TODO - get the user name from the token and populate
                if (expiryFlag == null || Boolean.valueOf(expiryFlag.toString()) == false){
                    UsernamePasswordAuthenticationToken newAuthentication =
                            new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    newAuthentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest   ));
                    SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                    log.info("ACTION=CHECK_AUTHORISATION AUTH_SET auth={}", newAuthentication);
                }

            }


        });


        filterChain.doFilter(httpServletRequest,httpServletResponse);

    }
}
