package com.shahtek.kkm.exception;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(String msg, Exception e) {

        super(msg + " because of " + e.toString());
    }
}
