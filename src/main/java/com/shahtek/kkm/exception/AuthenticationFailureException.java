package com.shahtek.kkm.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthenticationFailureException extends AuthenticationException {

    public AuthenticationFailureException(String msg) {
        super(msg);
    }

    public AuthenticationFailureException(String msg, Exception e) {

        super(msg + " because of " + e.toString());
    }
}
