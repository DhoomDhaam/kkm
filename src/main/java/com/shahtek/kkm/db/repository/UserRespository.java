package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRespository extends CrudRepository<User, String> {
}
