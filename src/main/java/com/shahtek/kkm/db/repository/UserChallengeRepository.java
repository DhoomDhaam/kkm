package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.ChallengeHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserChallengeRepository extends CrudRepository<ChallengeHistory, Integer> {
}
