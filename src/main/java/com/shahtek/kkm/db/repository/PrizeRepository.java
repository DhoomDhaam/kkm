package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Prize;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrizeRepository extends CrudRepository<Prize, Integer> {
}
