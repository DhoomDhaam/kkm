package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Challenge;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface ChallengeRepository extends CrudRepository<Challenge, String> {


    Stream<Challenge> findChallengeByStartTimeBetween(long startTime, long endTime);
}
