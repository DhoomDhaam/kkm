package com.shahtek.kkm.db.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class Competition {

    @Id
    @Column(name = "competition_id")
    @GeneratedValue(strategy = IDENTITY)
    private Integer competitionID;

    private String name;

    private String description;

    private String image;

}
