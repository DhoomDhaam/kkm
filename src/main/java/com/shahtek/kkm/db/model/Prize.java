package com.shahtek.kkm.db.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Prize {


    @Id
    @Column(name = "prize_id")
    @GeneratedValue(strategy = IDENTITY)
    private Integer prizeId;

    private String name;

    private int amount;

    private String image;

    @ManyToOne
    @JoinColumn(name="challenge_id", nullable=false)
    private Challenge challenge;
}
