package com.shahtek.kkm.db.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class Challenge {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "challenge_id")
    private String challengeID;

    private String name;

    private String description;

    private String image;

    private String code;

    @Column(name = "start_time")
    private long startTime;

    @Column(name= "entry_fee")
    private int entryFee;

    private String status;

    @ManyToOne
    @JoinColumn(name="competition_id", nullable=false)
    private Competition competition;


}


