package com.shahtek.kkm.db.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChallengeHistory {

    @Id
    @Column(name = "history_id")
    @GeneratedValue(strategy = IDENTITY)
    private Integer historyId;

    @ManyToOne
    @JoinColumn(name="email", nullable=false)
    private User user;

    @ManyToOne
    @JoinColumn(name="challenge_id", nullable=false)
    private Challenge challenge;

    private int score;

    @Column(name="start_time")
    private long startTime;

    @Column(name="end_time")
    private long endTime;
}


