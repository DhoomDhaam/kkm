package com.shahtek.kkm.util;


import com.shahtek.kkm.exception.NotFoundException;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Optional;

public class ResourceFileUtil {

    public static InputStream readResourceAsStream(String resource){

        return Optional.ofNullable(ResourceFileUtil.class.getResourceAsStream(resource)).orElseThrow(() -> new NotFoundException("File not found : " + resource));

    }

    @SneakyThrows
    public static String readResourceAsString(String resource){

        return IOUtils.toString(readResourceAsStream(resource), Charset.defaultCharset());

    }
}
