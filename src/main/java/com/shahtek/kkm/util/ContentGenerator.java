package com.shahtek.kkm.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;


@Component
@Slf4j
public class ContentGenerator {

    private Configuration cfg;

    public ContentGenerator(){

        cfg = new Configuration();
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(this.getClass(), "/");

    }

    @SneakyThrows
    public String convert(String template, Map<String,Object> data){
        String content = null;

        log.info("action=generate_content template={}, data={}", template, data);
        Template templateObj = cfg.getTemplate(template);
        try(Writer writer = new StringWriter()) {
            templateObj.process(data, writer);
            content = writer.toString();
        }
        log.info("action=generate_content template={}, data={} result={}", template, data, content);
        return content;
    }
}
