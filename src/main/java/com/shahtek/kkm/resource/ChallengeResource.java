package com.shahtek.kkm.resource;

import com.google.api.client.util.Lists;
import com.shahtek.kkm.config.AppConfiguration;
import com.shahtek.kkm.db.model.Challenge;
import com.shahtek.kkm.db.repository.ChallengeRepository;
import com.shahtek.kkm.exception.NotFoundException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ChallengeResource {

    private ChallengeRepository challengeRepository;

    public ChallengeResource(ChallengeRepository challengeRepository) {
        this.challengeRepository = challengeRepository;
    }

    @PostMapping("/api/v1/challenges")
    public ResponseEntity<Challenge> createChallenge(@RequestBody Challenge challenge) {
        log.info("action=create_challenge challenge={}", challenge);

        Challenge result = challengeRepository.save(challenge);
        log.info("action=create_challenge result={}", result);

        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }


    @GetMapping("/api/v1/challenges")
    public ResponseEntity<List<Challenge>> findAllChallenge() {
        return ResponseEntity.ok().body(Lists.newArrayList(challengeRepository.findAll()));

    }

    @GetMapping("/ap1/v1/challenges/{id}")
    public ResponseEntity<Challenge> findChallengeById(@PathVariable String id){
        log.info("action=retrieve_challenge id={}", id);

        Challenge challenge = challengeRepository.findById(id).orElseThrow(
                () -> new NotFoundException(String.format("Challenge with ID=%s not found", id)));
        log.info("action=retrieve_challenge result={}", challenge);
        return ResponseEntity.ok(challenge);
    }

    @GetMapping("ap1/v1/challenges/between/{startTIme}/{endTime}")
    @SneakyThrows
    @Transactional
    public ResponseEntity<List<Challenge>> findChallengesBetween(@PathVariable String startTime, @PathVariable String endTime){

        log.info("action=retrieve_challenge_between startTime={} endTime={}", startTime, endTime);
        SimpleDateFormat sdf = new SimpleDateFormat(AppConfiguration.DATE_FORMT);

        Date fromDate = sdf.parse(startTime);
        Date toDate = sdf.parse(endTime);


        List<Challenge> challenges = challengeRepository.findChallengeByStartTimeBetween(fromDate.getTime(), toDate.getTime()).collect(Collectors.toList());

        log.info("action=retrieve_challenges_between result_size={}", challenges.size());
        return Optional.ofNullable(challenges).filter(f ->f.size()>0).map(v -> ResponseEntity.ok(v)).
                orElseThrow(() -> new NotFoundException(String.format("No challenges found between startdate=%s toDate=%s",startTime, endTime)));
    }


}
