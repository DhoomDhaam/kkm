package com.shahtek.kkm.resource;

import com.shahtek.kkm.auth.AuthTokenManager;
import com.shahtek.kkm.auth.KKMAuthenticationManager;
import com.shahtek.kkm.auth.KKMUserDetailsService;
import com.shahtek.kkm.exception.AuthenticationFailureException;
import com.shahtek.kkm.model.LoginModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationResource {

    private final KKMAuthenticationManager authenticationManager;

    private final AuthTokenManager authTokenManager;

    private final KKMUserDetailsService userDetailsService;

    public AuthenticationResource(KKMAuthenticationManager authenticationManager, AuthTokenManager authTokenManager, KKMUserDetailsService userDetailsService){
        this.authenticationManager = authenticationManager;
        this.authTokenManager = authTokenManager;
        this.userDetailsService = userDetailsService;

    }


    @PostMapping("/api/v1/authenticate")
    public ResponseEntity<String> authenticate(@RequestBody LoginModel loginModel){
        log.info("ACTION=AUTHENTICATE user={}", loginModel);

        authenticate(loginModel.getUserName(), loginModel.getPassword());

        UserDetails userDetails = userDetailsService.loadUserByUsername(loginModel.getUserName());

        String token = authTokenManager.createToken(loginModel.getUserName());

        log.info("ACTION=AUTHENTICATE token={}", token);

        return ResponseEntity.ok(token);
    }

    private void authenticate(String userName, String password){
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));

        }catch(Exception ex){
            log.error("ACTION_AUTHENTICATE ERROR", ex);
            throw new AuthenticationFailureException("Authentication Failed", ex);
        }

    }
}
