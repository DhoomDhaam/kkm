package com.shahtek.kkm.resource;


import com.shahtek.kkm.db.model.Competition;
import com.shahtek.kkm.db.repository.CompetitionRepository;
import com.shahtek.kkm.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/competitions")
public class CompetitionResource {

    private CompetitionRepository competitionRepository;

    public CompetitionResource(CompetitionRepository competitionRepository){
        this.competitionRepository = competitionRepository;
    }


    @GetMapping("/")
    public ResponseEntity<List<Competition>> retrieveCompetitions(){

        List<Competition> competitions =  competitionRepository.findAll();

        log.info("action=retrieve_competition result={}", competitions.size());

        return ResponseEntity.status(HttpStatus.OK).body(competitions);

    }

    @GetMapping("/{id}")
    public ResponseEntity<Competition> retrieveCompetition(@PathVariable Integer id){

        log.info("action=retrieve_competition for id={}", id);
        Competition competition = competitionRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Competition not found with id=%s",id)));

        log.info("action=retrieve_competition for id={} result={}", id, competition);
        return ResponseEntity.status(HttpStatus.OK).body(competition);

    }

    @PostMapping("/")
    public ResponseEntity<Competition> createCompetition(@RequestBody Competition competition){
        log.info("action=create_competition competition={}", competition);

        Competition result = competitionRepository.save(competition);
        log.info("action=create_competition created={}", result);

        return ResponseEntity.status(HttpStatus.CREATED).body(competition);

    }


}
