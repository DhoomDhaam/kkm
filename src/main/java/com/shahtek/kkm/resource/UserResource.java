package com.shahtek.kkm.resource;

import com.shahtek.kkm.db.model.User;
import com.shahtek.kkm.db.repository.UserRespository;
import com.shahtek.kkm.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserResource {

    private UserRespository userRespository;

    public UserResource(UserRespository userRespository){
        this.userRespository = userRespository;
    }

    @GetMapping("/api/v1/users/{id}")
    public ResponseEntity<User> findUser(@PathVariable("id") String userId){

        log.info("action=find_user user_id={}", userId);
        User user = userRespository.findById(userId).orElseThrow(
                () -> new NotFoundException(String.format("User with userid=%s not found",userId)));

        log.info("action=find_user user_id={} user={}", userId, user);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/api/v1/users")
    public ResponseEntity<User> createUpdateUser(@RequestBody User user){

        log.info("action=create_user user_id={}", user);
        User result = userRespository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }


}
