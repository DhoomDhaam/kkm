package com.shahtek.kkm.config;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

@Configuration
@Slf4j
public class ApplicationBeanConfig {


//    @Bean
    @SneakyThrows
    public Gmail gmail(MailProperties mailProperties, ResourceLoader rs ){

        final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        Gmail gmail = new Gmail.Builder(httpTransport, jsonFactory, getCredentials(mailProperties, httpTransport, jsonFactory, rs))
                .setApplicationName(mailProperties.getApp())
                .build();

        return gmail;

    }

    private  Credential getCredentials(MailProperties mailProperties, final NetHttpTransport HTTP_TRANSPORT,
                                       JsonFactory jsonFactory, ResourceLoader rs ) throws IOException {
        final List<String> SCOPES = Arrays.asList(GmailScopes.GMAIL_SEND);
        final String TOKENS_DIRECTORY_PATH = "tokens";

        String finalResourcePath = ResourceLoader.CLASSPATH_URL_PREFIX+mailProperties.getCredfile();

        log.info("resource ={}", finalResourcePath);

        InputStream in = rs.getResource(finalResourcePath).getInputStream();
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " +finalResourcePath);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, jsonFactory, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
}
