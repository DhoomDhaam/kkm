package com.shahtek.kkm.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "auth")
@Configuration
@Data
public class AuthenticationProperties {

    private boolean enabled;
    private String secret;
    private int expiry;
    private String issuer;
}
