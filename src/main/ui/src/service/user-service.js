import axios from 'axios'

const API_URL = '';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*' 
axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS, PUT, PATCH, DELETE' 
axios.defaults.headers.common['Access-Allow-Allow-Origin'] = 'X-Requested-With, content-type' 
axios.defaults.headers.common['Access-Control-Allow-Credentials'] = 'true' 

class UserService{

    constructor(){
        this.config = {
            headers:
              {
                'noCache': 'true',
                'responseType': 'Blob',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
                'Access-Control-Allow-Headers': 'X-Requested-With, content-type',
                'Access-Control-Allow-Credentials': 'true'
      
              }
          };
    }

    

    loginUser( user){
        console.log('action=login_user name='+user.userName);
        return axios.post(`${API_URL}/api/v1/authenticate`, user);
        
    }


}

export default new UserService()
