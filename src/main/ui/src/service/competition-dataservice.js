import axios from 'axios'


const API_URL = `/api/v1/competitions`;
const GET_ALL_COMPETITIONS = `${API_URL}/`

class CompetitionDataService {

    getHeaders() {
        const token = localStorage.getItem('token');
        return {
            headers: {
                'Authorization': `${token}`,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
                'Access-Control-Allow-Headers': 'X-Requested-With, content-type',
                'Access-Control-Allow-Credentials': 'true'
            },
        };
    }

    retrieveCompetitions() {
        return axios.get(`${GET_ALL_COMPETITIONS}`, this.getHeaders());
    }
    getCompetitionDetails(id) {
        console.log("Get specific challenge details WITH ID = {}", `${API_URL}/${id}`);
        return axios.get(`${API_URL}/${id}`, this.getHeaders());

    }
}

export default new CompetitionDataService()