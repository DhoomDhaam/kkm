import logo from './logo.svg';
import React, { Component } from 'react';
import { Button} from 'react-bootstrap';
import './App.css';
import Dashboard from './component/dashboard'
import DashBoardComponent from './component/dashboard';
import HeaderComponent from './component/header';
import HeaderDetail from './component/header'
import { BrowserRouter, Route, Switch ,withRouter,Link, useHistory, NavLink} from 'react-router-dom';
import Login from './component/login'
import HowToPlay from './component/howtoplay'
import RecoverPassword from './component/recoverpassword'
import Register from './component/register';
import OverviewComponent from './component/overview';
import MySpace from './component/myspace';
import { ToastContainer, toast } from 'react-toastify';


class App extends Component {

  navigateToLogin = () => {
    this.context.router.push('/login')
  };

  render() {
    return (
      <div className="App">
       
        <main>
        <BrowserRouter>
        <div className="App-header">
                    {/* <img src='./app.jpg'/> */}
                    { <h1 className="App-title" >Khel Khel Mei !</h1> }
                    <div className="content-middle">
                     
                    </div>
                    <div className="content-right">
                    <Link to="/" className="sub-heading" >Home</Link>
                    <Link to="/howToPlay" className="sub-heading" >How to Play</Link>
                    <Link to="/login" ><Button variant="danger" >Login</Button></Link>
                    <Link to="/register" ><Button variant="danger" >Register</Button></Link>
                    <ToastContainer
                      position="top-right"
                      autoClose={2000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                      className="toaster"
                      />
                    </div>
        </div>
        <ColoredLine color="#dc3545"/>
            <Switch>
              <Route path='/login' component={Login} />
              <Route path='/howToPlay' component={HowToPlay} />
              <Route path='/register' component={Register} />
              <Route path='/forgotPassword' component={RecoverPassword} />
              <Route path='/mySpace' component={MySpace} />
              <Route path='/' component={DashBoardComponent} />
              
            </Switch>
          </BrowserRouter>
        </main>
    </div>
   
    
    );
  }


}

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 1
      }}
  />
);

export default App;
