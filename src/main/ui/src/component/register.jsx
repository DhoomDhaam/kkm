import { Component } from "react";
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {Link} from 'react-router-dom';
import "./login.css";

class Register extends Component{

    constructor(){
        super()
        this.state = {
            name : '', 
            email : '',
            password : '',
            phone: ''
          }

    }

    render(){
        return(
            <div className="Login">
            <Form>
                <h1>Register Account</h1>
                <Form.Group size="lg" controlId="email">
                <Form.Control
                    autoFocus
                    type="name"
                    placeholder="Full Name"
                    value={this.state.name}
                />
                <Form.Group size="lg" controlId="email"></Form.Group>
                    <Form.Control
                        autoFocus
                        type="email"
                        placeholder="Email"
                        value={this.state.email}
                    />
                </Form.Group>
                <Form.Group size="lg" controlId="password">
                    <Form.Control
                        type="password"
                        value={this.state.password}
                        placeholder="Password"
                    />
                </Form.Group>
                <Form.Group size="lg" controlId="phone">
                <Form.Control
                    type="phone"
                    value={this.state.phone}
                    placeholder="Phone"
                />
                </Form.Group>
                <Button variant="danger" block size="lg" type="submit" >
                    {/* disabled={!validateForm()}> */}
                Register
                </Button>
                
            </Form>
            </div>
        );
    }

}

export default Register;