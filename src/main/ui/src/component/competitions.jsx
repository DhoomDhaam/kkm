import { Component, useState } from "react";
import Modal from 'react-bootstrap/Modal'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'
import { Button } from 'react-bootstrap';
import ModalHeader from "react-bootstrap/esm/ModalHeader";
import CompetitionDataService from '../service/competition-dataservice'
import Carousel from 'react-bootstrap/Carousel'
import { Redirect, useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';

class CompetitionComponent extends Component {


    state = {
        show: false,
        selectedCompetition: [],
        redirectFlag: false,
        competitions: [],
        message: null
    };

    constructor(props) {
        super(props);
        this.retrieveCompetitions = this.retrieveCompetitions.bind(this);
    }


    handleClose = (e) => { this.setState({ show: false }); };

    handlePlay = (e, selected) => {
        if (localStorage.getItem('token') == null) {
            toast.error('no token present - please login');
            this.setState({ redirectFlag: true });
        } else {

            console.log('play mode on ', this.state.selectedCompetition['competitionID']);
        }

    };

    handleShow = (e, competition) => {
        this.getCompetitionDetails(competition['competitionID']);
        this.setState({ show: true });
    };


    getCompetitionDetails(id) {
        CompetitionDataService.getCompetitionDetails(id)
            .then(response => {
                this.setState({ selectedCompetition: response.data })
            })
            .catch(error => {
                if (error.response.data.status == 401)
                    this.setState({ redirectFlag: true });
            })
    }

    componentDidMount() {
        this.retrieveCompetitions();
    }

    retrieveCompetitions() {
        CompetitionDataService.retrieveCompetitions()
            .then(response => {
                this.setState({ competitions: response.data })
            })
            .catch(error => {
                if (error.response.data.status == 401) {
                    this.setState({ redirectFlag: true });
                }
            })
    }

    render() {

        let isRedirectEnabled = this.state.redirectFlag;
        if (isRedirectEnabled) {
            return <Redirect to='/login' />;
        }
        return (
            <>
                <div className="parent-div container">
                    <div className="row">
                        <div className="col">
                            <div className="competition-conatainer">
                                <h2 className="competition-child">Featured Competitions</h2>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        {
                            this.state.competitions.map(
                                competition =>
                                    <div mb="30" mt="0" className="col-md-4 col-12">
                                        <a className="single-competitions comp-bg-1" onClick={e => {
                                            this.handleShow(e, competition);
                                            this.setState({ selectedCompetition: competition })
                                        }}
                                            style={{ backgroundImage: "url(" + competition.image + ")" }}>
                                            <div className="price-concat">
                                                <div className="strike-through">{competition.competitionID}</div>
                                                <div className="actual-price">{competition.competitionID}</div>
                                            </div>
                                        </a>
                                        <p className="competition-title">{competition.name}</p>
                                    </div>
                            )
                        }

                    </div>

                    <Modal show={this.state.show} onHide={this.handleClose} aria-labelledby="example-modal-sizes-title-lg" size="lg" >
                        <ModalHeader className="Modal-header" closeButton>
                            <ModalTitle> {this.state.selectedCompetition.name}</ModalTitle>
                        </ModalHeader>
                        <ModalBody style={{ color: "black" }}>

                            {
                                this.state.selectedCompetition ?
                                    (
                                        <>

                                            {renderChallengeDetails(this.state.selectedCompetition)}
                                            <div class="kVZCfy">
                                                <h2 class="bAfdJD">Description</h2>
                                                <pre>
                                                    {this.state.selectedCompetition['name']}
                                                </pre>
                                            </div>
                                        </>
                                    )
                                    :
                                    (this.state)
                            }

                        </ModalBody>
                        <ModalFooter>
                            <Button variant="secondary" onClick={this.handleClose}>Close</Button>
                            <Button variant="primary" onClick={this.handlePlay}>Play</Button>
                        </ModalFooter>
                    </Modal>

                </div>
            </>
        )
    }
}


function renderChallengeDetails(competition) {
    const carousels = []
    for (let i = 0; i < 10; i++) {

        carousels.push(<Carousel.Item>
            <img
                className="d-block w-100"
                src={`${competition.image}`}
                alt="{{i}} slide"
            />
            <Carousel.Caption>
                <h3>{competition.description}</h3>
            </Carousel.Caption>
        </Carousel.Item>);
    }
    return (
        <Carousel>
            {carousels}
        </Carousel>
    );
}



export default CompetitionComponent