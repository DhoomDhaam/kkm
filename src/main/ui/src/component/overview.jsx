import { Component } from "react";
// get our fontawesome imports
import { faAngleDoubleRight, faTrophy, faTicketAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class OverviewComponent extends Component {

    render() {
        return (
            <>
            <div className="parent-div"><h2 class="child-div">How it works</h2></div>

            <div className="row">
                <div mmb="30" className="col-md-4 col-12 row-class ">
                    <FontAwesomeIcon icon={faAngleDoubleRight} className="font-awesome"/>
                    <p className="p-text">1 - Pick a prize</p>
                    <p className="p-second-text"> We provide you with your dream car, amazing motorbikes, cash, laptops and more. Browse our Best selling prices and pick a competition.</p>
                </div>

                <div mmb="30" className="col-md-4 col-12 row-class ">
                    <FontAwesomeIcon icon={faTicketAlt} className="font-awesome"/>
                    <p className="p-text">2 - Select your tickets</p>
                    <p className="p-second-text"> Use the Lucky Dip or the custom ticket selector to select the letter and ticket number of your choice to enter the competition.</p>
                </div>

                <div mmb="30" className="col-md-4 col-12 row-class ">
                    <FontAwesomeIcon icon={faTrophy} className="font-awesome"/>
                    <p className="p-text">3 - Daily winner</p>
                    <p className="p-second-text"> Join the Winners Podium and make your dreams a reality. You have to be in it to win it!</p>
                </div>

            </div>
            </>
        )
    }
}

export default OverviewComponent