import { Component } from "react";
import CompetitionComponent from "./competitions";
import OverviewComponent from "./overview";


class DashBoardComponent extends Component {
    render() {
        return (
            <>
                <OverviewComponent />
                <br></br>
                <br></br>
                <CompetitionComponent />
            </>
        )
    }
}

export default DashBoardComponent