import { Component } from "react";
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {Link} from 'react-router-dom';
import "./login.css";
import UserService from "../service/user-service";
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Login extends Component{

    constructor(){
        super()
        this.state = {
            email: '',
            password: '',
            loginSuccess: false
          }

        this.validateForm = this.validateForm.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }

   validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0
   }

   loginUser(event){

       event.preventDefault();
       let loginModel = {
        userName: this.state.email,
        password: this.state.password
    }

    UserService.loginUser(loginModel).then(response => {
        console.log(response)
        localStorage.setItem('token', response.data);
        const { userData } = response.data;
        this.setState({ loginSuccess: true });

    }).catch(error => {
        console.log(error);
        toast.error('Invalid user id or password!', {});
        this.setState.loginSuccess = false;

    });
   }

    render(){
        let isRedirectEnabled = this.state.loginSuccess;
        if (isRedirectEnabled) {
            return <Redirect to='/' />;
        }

        return (
            <div className="Login">

                <Form onSubmit={this.loginUser}>
                    <h1>Login to your Account</h1>
                    <Form.Group size="lg" controlId="email">
                        <Form.Control
                            autoFocus
                            type="email"
                            placeholder="Email"
                            value={this.state.userName}
                            onChange={(event) => this.setState({ email: event.target.value })}
                        />
                    </Form.Group>
                    <Form.Group size="lg" controlId="password">
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={(event) => this.setState({ password: event.target.value })}
                        />
                        <div className="right-justified"><Link to="/forgotPassword" style={{ color: "grey" }}>
                            Forgot Password</Link></div>{this.state.loginSuccess}
                    </Form.Group>
                    <Button variant="danger" block size="lg" type="submit" disabled={!this.validateForm}>

                        Login
                </Button>
                    <div className="right-justified" ><Link to="/register" style={{ color: "grey" }}>Don't have an account? Register</Link></div>

                </Form>

            </div>

        );

        
    }


}

export default Login