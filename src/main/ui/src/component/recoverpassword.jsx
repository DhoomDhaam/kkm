import { Component } from "react";
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {Link} from 'react-router-dom';

class RecoverPassword extends Component{


    constructor(){
        super()
        this.state = {
            email: '',
          }

    }

    render(){
        return(
            <div className="Login">
                <Form>
                    <h1>Recover Password</h1>
                    <Form.Group size="lg" controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                autoFocus
                                type="email"
                                value={this.state.email}
                                //onChange={(e) => setEmail(e.target.value)}
                            />
                    </Form.Group>
                    <Button variant="danger" block size="lg" type="submit" >
                        {/* disabled={!validateForm()}> */}
                    Send Recovery Email
                    </Button>
                </Form>
            </div>
            
           
        );


    }

}

export default RecoverPassword;