class Competition extends ReactAxiom.Model {

    static defaultState() {
        return {
          id: null,
          url: '',
          thumbnailUrl: ''
        };
      }

}