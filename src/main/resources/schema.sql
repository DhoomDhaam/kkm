create table if not exists COMPETITION
(
  competition_id integer not null auto_increment,
  name varchar(255) not null,
  description varchar(255) not null,
  image varchar(255),
  primary key (competition_id)
);

create table if not exists CHALLENGE
(
  challenge_id integer not null auto_increment,
  name varchar(255) not null,
  description varchar(255) not null,
  image varchar(255),
  code varchar(100),
  start_time timestamp,
  status varchar(20),
  entry_fee integer,
  competition integer not null,
  primary key (challenge_id),
  constraint fk_competition foreign key(competition) references COMPETITION(competition_id)
);

create table if not exists PRIZE
(
  prize_id integer not null auto_increment,
  challenge integer not null,
  name varchar(50) not null,
  amount integer,
  primary key (prize_id),
  constraint fk_challenge foreign key(challenge) references CHALLENGE(challenge_id)

);
create table if not exists ROLE
(
  role_name varchar(255) not null,
  role_description varchar(255) not null,
primary key (role_name)
);

create table if not exists USER
(
    email VARCHAR(255) not null,
    first_name VARCHAR(255) not null,
    last_name VARCHAR(255) not null,
    password VARCHAR(255) not null,
    role_name VARCHAR(255) not null,
    created_on timestamp default CURRENT_TIMESTAMP,
    status char default 'C',
    PRIMARY KEY (email),
constraint fk_role foreign key(role_name) references role(role_name)
);

create table if not exists CHALLENGE_HISTORY
(
  history_id integer not null auto_increment,
  user varchar(255),
  challenge integer,
  score integer,
  start_time timestamp,
  end_time timestamp,
  primary key (history_id),
  constraint  fk_user_challenge_history foreign key(user) references USER(email),
  constraint fk_challenge_history foreign key(challenge) references CHALLENGE(challenge_id)

);

