package com.shahtek.kkm.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ContentGeneratorTest {

    @Test
    public void testContentGenerator(){

        ContentGenerator underTest = new ContentGenerator();

        Map<String, Object> input = new HashMap(){{
            put("a", "a1");
            put("b", "b1");
        }};

        String result = underTest.convert("test.ftl", input);
        assert result != null;
        assert result.contains("a1");

    }
}
