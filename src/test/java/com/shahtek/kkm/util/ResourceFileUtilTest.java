package com.shahtek.kkm.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ResourceFileUtilTest {

    @Test
    void readResourceAsStream() {
        InputStream is = ResourceFileUtil.readResourceAsStream("/credentials.json");
        log.info("is={}", is);
        assert is != null;
        IOUtils.closeQuietly(is);
    }

    @Test
    void readResourceAsString() {

        String data = ResourceFileUtil.readResourceAsString("/credentials.json");
        log.info("data={}", data);
        assert data != null;
        assert data.length() > 0;
    }
}