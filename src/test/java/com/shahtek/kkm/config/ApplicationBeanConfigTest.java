package com.shahtek.kkm.config;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.shahtek.kkm.notification.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class ApplicationBeanConfigTest {


    @Autowired
    Gmail gmail;

    @Test
    void gmail() {
        assert gmail != null;

        // test notification

        NotificationService notificationService = new NotificationService();

        Message message = notificationService.sendEmail("ashish.tiwari2005@gmail.com",
                "ashish.tiwari@shashtek.co.uk",
                "testing",
                "test template", null);

        assert message != null;
        log.info("final message={}", message);


    }
}