package com.shahtek.kkm.resource;

import com.shahtek.kkm.db.model.Competition;
import com.shahtek.kkm.db.repository.CompetitionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class CompetitionControllerTest {


    @Test
    void retrieveCompetitions() {

        CompetitionRepository compRepo = Mockito.mock(CompetitionRepository.class);

        Mockito.when(compRepo.findAll()).thenReturn(Arrays.asList(Competition.builder().competitionID(1).build()));
        CompetitionResource underTest = new CompetitionResource(compRepo);

        ResponseEntity<List<Competition>> result = underTest.retrieveCompetitions();

        assert result != null;

        List<Competition> competitions = result.getBody();

        assert competitions != null;
        assert  competitions.size() == 1;
    }

    @Test
    void retrieveCompetitionById() {

        CompetitionRepository compRepo = Mockito.mock(CompetitionRepository.class);

        Mockito.when(compRepo.findById(1)).thenReturn(Optional.of(Competition.builder().competitionID(1).build()));
        CompetitionResource underTest = new CompetitionResource(compRepo);

        ResponseEntity<Competition> result = underTest.retrieveCompetition(1);

        assert result != null;

        Competition competitions = result.getBody();

        assert competitions != null;
        assert  competitions.getCompetitionID() == 1;
    }

    @Test
    void saveCompetition() {

        CompetitionRepository compRepo = Mockito.mock(CompetitionRepository.class);

        Competition comp = Competition.builder().competitionID(1).build();
        Mockito.when(compRepo.save(comp)).thenReturn(comp);
        CompetitionResource underTest = new CompetitionResource(compRepo);

        ResponseEntity<Competition> result = underTest.createCompetition(comp);

        assert result != null;

        Competition competitions = result.getBody();

        assert competitions != null;
        assert  competitions.getCompetitionID() == 1;
    }


}