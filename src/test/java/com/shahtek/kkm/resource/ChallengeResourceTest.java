package com.shahtek.kkm.resource;

import com.shahtek.kkm.db.model.Challenge;
import com.shahtek.kkm.db.repository.ChallengeRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


@Slf4j
class ChallengeResourceTest {

    ChallengeRepository challengeRepository = Mockito.mock(ChallengeRepository.class);

    ChallengeResource underTest = new ChallengeResource(challengeRepository);

    @Test
    void createChallenge() {

        Challenge challenge = Challenge.builder().challengeID("123").build();

        Mockito.when(challengeRepository.save(challenge)).thenReturn(challenge);

        ResponseEntity<Challenge> response = underTest.createChallenge(challenge);

        assert response != null;
        assert response.getBody().getChallengeID().equalsIgnoreCase("123");

    }

    @Test
    void findAllChallenge() {
        Challenge challenge = Challenge.builder().challengeID("123").build();

        Mockito.when(challengeRepository.findAll()).thenReturn(Arrays.asList(challenge));

        ResponseEntity<List<Challenge>> response = underTest.findAllChallenge();

        assert response != null;
        assert response.getBody().size() > 0;
    }

    @Test
    void findChallengeById() {
        Challenge challenge = Challenge.builder().challengeID("123").build();

        Mockito.when(challengeRepository.findById("123")).thenReturn(Optional.of(challenge));

        ResponseEntity<Challenge> response = underTest.findChallengeById("123");

        assert response != null;
        assert response.getBody().getChallengeID().equalsIgnoreCase("123");
    }

    @Test
    void findChallengesBetween() {
        Challenge challenge = Challenge.builder().challengeID("123").build();

        Mockito.when(challengeRepository.findChallengeByStartTimeBetween(Mockito.anyLong(), Mockito.anyLong()))
               .thenReturn(Arrays.asList(challenge).stream());

        ResponseEntity<List<Challenge>> response = underTest.findChallengesBetween("2000-04-12", "2020-04-12");

        assert response != null;
        assert response.getBody().size() > 0;

    }
}