package com.shahtek.kkm.resource;

import com.shahtek.kkm.db.model.User;
import com.shahtek.kkm.db.repository.UserRespository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import java.util.Optional;


class UserResourceTest {


    private UserRespository userRespository = Mockito.mock(UserRespository.class);
    private UserResource underTest = new UserResource(userRespository);

    @Test
    public void testUserCreation(){

        User user = User.builder().email("123").build();

        Mockito.when(userRespository.save(user)).thenReturn(user);

        ResponseEntity<User> response = underTest.createUpdateUser(user);

        assert response != null;
        assert response.getBody().getEmail().equalsIgnoreCase("123");

    }

    @Test
    public void testUserRetrieval(){
        User user = User.builder().email("1.com").build();
        Mockito.when(userRespository.findById("a.com")).thenReturn(Optional.of(user));
        ResponseEntity<User> response = underTest.findUser("a.com");

        assert response != null;
        assert response.getBody().getEmail().equalsIgnoreCase("1.com");


    }

    

}