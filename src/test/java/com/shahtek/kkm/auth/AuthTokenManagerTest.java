package com.shahtek.kkm.auth;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
@Slf4j
class AuthTokenManagerTest {


    @Autowired
    AuthTokenManager underTest;


    @Test
    void createToken() {
        assert underTest != null;

        String token = underTest.createToken("123");
        log.info("token={}", token);
        assert token != null;

        Map<String, Object> result = underTest.validateToken(token);
        log.info("result={}", result);

        assert  result != null;
    }

    @Test
    void parseToken() {
    }
}