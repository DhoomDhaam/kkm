package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Competition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CompetitionRepositoryTest {

    @Autowired
    CompetitionRepository underTest;

    @Test
    void testCompetitionInsert() {

        assert underTest != null;

        Competition competition = Competition.builder().name("some").description("desc").image("image").build();

        Competition result = underTest.save(competition);
        assert result.getName().equalsIgnoreCase("some");

        assert underTest.findAll().spliterator().estimateSize() > 0;

    }

}