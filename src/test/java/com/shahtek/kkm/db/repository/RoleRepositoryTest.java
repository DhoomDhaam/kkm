package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Role;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class RoleRepositoryTest {

    @Autowired
    RoleRepository underTest;

    @Test
    public void testRoleSaveAndRetrieval(){

        Role role = Role.builder().roleName("test").roleDescription("testDesc").build();

        Role response = underTest.save(role);

        assert response != null;
        assert response.getRoleName() == "test";

        assert underTest.findAll().spliterator().estimateSize() == 1l;

    }

}