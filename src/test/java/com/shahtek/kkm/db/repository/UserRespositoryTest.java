package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Role;
import com.shahtek.kkm.db.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserRespositoryTest {


    @Autowired
    UserRespository underTest;

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void testSaveAndRetrieve(){

        Role role = Role.builder().roleName("User").build();

        Role resultingRole = roleRepository.save(role);

        User user = User.builder().email("a.com").roleName(resultingRole).build();

        User response = underTest.save(user);

        assert response != null;
        assert response.getEmail().equalsIgnoreCase("a.com");

        assert underTest.findAll().spliterator().estimateSize() == 1l;



    }

}