package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Challenge;
import com.shahtek.kkm.db.model.Competition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;
import java.util.stream.Stream;


@SpringBootTest
class ChallengeRepositoryTest {

    @Autowired
    ChallengeRepository underTest;

    @Autowired
    CompetitionRepository competitionRepository;

    @Test
    public void testSaveAndRetrieve(){
        Competition comp = Competition.builder().competitionID(2).build();
        Competition resultComp = competitionRepository.save(comp);

        Challenge challenge = Challenge.builder().description("test").competition(resultComp).build();

        Challenge result = underTest.save(challenge);

        assert result != null;
        assert result.getDescription().equalsIgnoreCase("test");
        assert underTest.findAll().spliterator().estimateSize() > 0l;

    }

    @Test
    @Transactional
    public void testBetweenQuery(){

        Competition comp = Competition.builder().competitionID(2).build();
        Competition resultComp = competitionRepository.save(comp);

        Challenge challenge = Challenge.builder().description("test").startTime(100).competition(resultComp).build();

        Challenge result = underTest.save(challenge);

        Stream<Challenge> resultStream = underTest.findChallengeByStartTimeBetween(0, System.currentTimeMillis());

        assert resultStream != null;
        assert resultStream.collect(Collectors.toList()).size() >0;


    }

}