package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserChallengeRepositoryTest {

    @Autowired
    UserChallengeRepository underTest;

    @Autowired
    CompetitionRepository competitionRepository;

    @Autowired
    ChallengeRepository challengeRepository;

    @Autowired
    UserRespository userRespository;

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void testSaveAndRetrieve(){
        Competition comp = Competition.builder().competitionID(2).build();
        Competition resultComp = competitionRepository.save(comp);

        Challenge challenge = Challenge.builder().description("test").competition(resultComp).build();

        Challenge temp = challengeRepository.save(challenge);


        Role role = Role.builder().roleName("User").build();

        Role resultingRole = roleRepository.save(role);

        User user = User.builder().email("a.com").roleName(resultingRole).build();

        User tempUser = userRespository.save(user);


        ChallengeHistory challengeHistory = ChallengeHistory.builder().challenge(challenge).user(tempUser).score(10).build();

        ChallengeHistory result = underTest.save(challengeHistory);

        assert result != null;
        assert result.getScore() == 10;
        assert underTest.findAll().spliterator().estimateSize() > 0l;


    }

}