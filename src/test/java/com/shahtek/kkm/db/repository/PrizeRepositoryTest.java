package com.shahtek.kkm.db.repository;

import com.shahtek.kkm.db.model.Challenge;
import com.shahtek.kkm.db.model.Competition;
import com.shahtek.kkm.db.model.Prize;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class PrizeRepositoryTest {

    @Autowired
    PrizeRepository underTest;

    @Autowired
    CompetitionRepository competitionRepository;

    @Autowired
    ChallengeRepository challengeRepository;

    @Test
    public void testSaveAndRetrieve(){
        Competition comp = Competition.builder().competitionID(2).build();
        Competition resultComp = competitionRepository.save(comp);

        Challenge challenge = Challenge.builder().description("test").competition(resultComp).build();

        Challenge result = challengeRepository.save(challenge);

        assert result != null;
        assert result.getDescription().equalsIgnoreCase("test");
        assert challengeRepository.findAll().spliterator().estimateSize() > 0l;

        Prize prize = Prize.builder().challenge(challenge).name("first").amount(1000).build();

        Prize prizeResult = underTest.save(prize);

        assert prizeResult != null;
        assert prizeResult.getAmount() == 1000;
        assert prizeResult.getName().equalsIgnoreCase("first");

    }

}